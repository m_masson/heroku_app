"use strict";
const express = require('express');
const routes = require('./routes/api');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');

// set up express app
const app = express();

// set port
var port = process.env.PORT || 8080
// connect to mongoDB
const MongoClient = require('mongodb').MongoClient;
mongoose.connect('mongodb+srv://admin:admin@cluster0-negtk.mongodb.net/test?retryWrites=true', { useNewUrlParser: true, useFindAndModify: false, useCreateIndex: true });
mongoose.Promise = global.Promise;


// Handle static file
app.use(express.static('public'));

// Use bodyparser for JSON Objects
app.use(bodyParser.json());

// Initialize routes
app.use('/api', routes);

// Error handling middleware
// Can take up to 4 parameters
app.use(function(err, req, res, next){
    //console.log(err);
    res.status(422).send({error: err.message});
});


// listen for request
// @process.env.port If external app has env variable setup for a port
app.listen(port, function() {
    console.log('Listening for requests');
});
